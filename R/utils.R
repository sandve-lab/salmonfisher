#' Convert to short species code name
#'
#' Given an acceptable species name, return the relevant short species code name.
#' @export
fish_species_short_name <- function(name) {
	if(name %in% c("Salmo salar", "Salmo_salar", "Ssal", "ssal")) {
		return ("Ssal")
	} else {
		return (warning("Species name is not acceptable"))
	}
}


### From https://rdrr.io/bioc/AnnotationDbi/src/R/utils.R
### Used at load time (in .onLoad) by all SQLite-based ann data packages.
dbFileConnect <- function(dbfile)
{
	if (!file.exists(dbfile))
		stop("DB file '", dbfile, "' not found")
	if (.Platform$OS.type == "unix") {
		DBI::dbConnect(RSQLite::SQLite(), dbname=dbfile, cache_size=64000L,
									 synchronous="off", flags=RSQLite::SQLITE_RO, vfs="unix-none")
	} else {
		## Use default 'vfs' on Windows.
		DBI::dbConnect(RSQLite::SQLite(), dbname=dbfile, cache_size=64000L,
									 synchronous="off", flags=RSQLite::SQLITE_RO)
	}
}

### Used at unload time (in .onUnload) by all SQLite-based ann data packages.
dbFileDisconnect <- function(dbconn)
{
	DBI::dbDisconnect(dbconn)
}
