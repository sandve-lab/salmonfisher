
### Install

Install and load `devtools`:

```
install.packages("devtools")
library(devtools)
```

Install `salmonfisher` from gitlab:

```
install_git('https://gitlab.com/sandve-lab/salmonfisher')
library(salmonfisher)
```

Required packages:  
```
library(DBI)
library(RSQLite)

# Or library(tidyverse) to load all below
library(tidyr)
library(dplyr)
library(dbplyr)
library(stringr)
```

### Functions

```
fish_annotation()
fish_GO()
fish_KEGG()
fish_expression()
```

### Example use

`fish_annotation`

Retrieve selected annotation info (gene, transcript, protein ids) for a given NCBI gene id of Atlantic salmon. 
Return only the longest transcript isoform per gene, and return a nonredundant table: only one result per NCBI gene id.
```
fish_annotation(
	id = "LOC106603563",
	id_type = "gene_name",
	species = "Ssal",
	select = c("gene_id", "transcript_id", "product")
)
```

Retrieve selected annotation info for features that overlap the given genome coordinates (in a specific format: chr:start:end) for Atlantic salmon. 
Return results for all transcript isoforms, and return a multiple results per NCBI gene id if they exist.
```
fish_annotation(
	id = "ssa04:47720000:47740000",
	id_type = "coords",
	species = "Ssal",
	select = c("gene_name", "transcript_id", "product", 
		"chromosome", "transcript_start", "transcript_end"),
	longest_isoform = FALSE,
	nonredundant = FALSE
)
```

`fish_GO`

For given Atlantic salmon NCBI gene id, return a table of all annotated GO ids in the biological process category in long format: one row per gene id : GO id.
```
fish_GO(
	id = "106603565",
	id_type = "gene_id",
	species = "Ssal"
)
```

For given Atlantic salmon gene ids, return a table of GO terms in short format: all GOs are joined into one string per gene.
```
fish_GO(
	id = c("106603565", "106566663", "106568467"),
	id_type = "gene_id",
	species = "Ssal",
	format = "short"
)
```

For given GO id, return a table of all Atlantic salmon gene entries with the given GO annotation.
```
fish_GO(
	id = "GO:0007165",
	id_type = "go_id",
	species = "Ssal"
)
```

`fish_KEGG`

For given KEGG pathway id, return a table of all Atlantic salmon gene ids with their KO ids annotated to that pathway.
```
fish_KEGG(
	id = "01040",
	id_type = "pathway_id",
	species = "Ssal"
)
```

For given KO id, return a table of all Atlantic salmon gene ids and pathway ids associated with that KO id.
```
fish_KEGG(
	id = "K10244",
	id_type = "KO",
	species = "Ssal"
)
```

`fish_expression`

For now only retrieve Atlantic salmon gene expression data, normalized as TPM values (Transcripts Per Million reads), from the 'Koop' tissue panel samples (See Lien et al 2006).
```
fish_expression(
	dataset = "tissue_panel_TPM",
	species = "Ssal"
)
```

Get tissue panel data for a subset of genes using gene ids.
```
fish_expression(
	dataset = "tissue_panel_TPM",
	id = c("106603565", "106566663", "106568467"),
	species = "Ssal"
)
```
